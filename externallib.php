<?php
/**
 * Created by PhpStorm.
 * User: maske
 * Date: 29/03/2016
 * Time: 14:52
 */

require_once($CFG->libdir . "/externallib.php");

class local_presenzewebservice_external extends external_api
{

    /************************************************

      //////////////////////////////////////
      // GET CURRENT LESSON FOR A STUDENT //
      //////////////////////////////////////

    *************************************************/

    /**
     * @return string
     * @throws coding_exception
     * @throws dml_missing_record_exception
     * @throws dml_multiple_records_exception
     * @throws invalid_parameter_exception
     * @throws moodle_exception
     * @throws restricted_context_exception
     *
     * Return json with the lesson that student should attend now,
     * if student have no lesson now, return json encode with error message
     */
    public static function get_lesson_by_beacon()
    {
        global $USER;
        global $DB;

        //Parameters validation
        $params = self::validate_parameters(self::get_lesson_by_beacon_parameters(),
            array()
        );

        //Context validation
        $context = get_context_instance(CONTEXT_USER, $USER->id);
        self::validate_context($context);

        //Capability checking
        if (!has_capability('moodle/user:viewdetails', $context)) {
            throw new moodle_exception('cannotviewprofile');
        }

        $courses = enrol_get_users_courses($USER->id, true, null, null);

        // Create courses statements for next db query
        $courses_string = '(';
        $first_time = true;
        foreach ($courses as $course) {
            if ($first_time) {
                $courses_string .= $course->id;
                $first_time = false;
            } else
                $courses_string .= ',' . $course->id;
        }
        $courses_string .= ')';

        $lesson = $DB->get_record_sql('SELECT * FROM {presenze_lezioni} WHERE lesson_end_date >= "' . date("Y-m-d H:i:s") . '" and course_id in ' . $courses_string . " order by lesson_start_date");

        if (!$lesson) {
            echo json_encode(array('error' => 'You don\'t have lessons in any course.'));
            return;
        }

        $date = (new DateTime($lesson->lesson_start_date))->format("H:i:s d-m-Y");
        $lesson->lesson_start_date = $date;
        $lesson->course_name = $courses[$lesson->course_id]->fullname;
        $beacon = $DB->get_record_sql('SELECT * FROM {presenze_beacons} WHERE id = ?', array($lesson->classroom_id));
        echo json_encode(array('beacon' => $beacon, 'lesson' => $lesson));
    }

    /**
     * Returns description of method parameters
     * @return external_function_parameters
     */
    public static function get_lesson_by_beacon_parameters()
    {
        return new external_function_parameters(
            array()
        );
    }

    /**
     * Returns description of method result value
     * @return external_description
     */
    public static function get_lesson_by_beacon_returns()
    {
        return new external_value(PARAM_TEXT, 'Return the lesson user should attend now and beacon mapped to that lesson.');
    }

    /************************************************
     *
     * /////////////////////////////////////
     * // MARK NEW PRESENCE FOR A STUDENT //
     * /////////////////////////////////////
     *************************************************/

    /**
     * @param $lesson_id
     * @return string
     * @throws coding_exception
     * @throws invalid_parameter_exception
     * @throws moodle_exception
     * @throws restricted_context_exception
     */
    public static function mark_presence($lesson_id)
    {

        global $USER;
        global $DB;

        //Parameters validation
        $params = self::validate_parameters(self::mark_presence_parameters(),
            array(
                'lesson_id' => $lesson_id)
        );

        //Context validation
        $context = get_context_instance(CONTEXT_USER, $USER->id);
        self::validate_context($context);

        //Capability checking
        if (!has_capability('moodle/user:viewdetails', $context))
            throw new moodle_exception('cannotviewprofile');

        // Check if the given lesson really exists
        if (!self::check_exist_lesson($lesson_id)) {
            echo json_encode(array('error' => 'Invalid lesson id'));
            return;
        }

        // Check if the given lesson really exists
        if (!self::check_time_to_mark($lesson_id)) {
            echo json_encode(array('error' => 'Can\'t mark presence before lesson starts or after 15 minutes from lesson begin'));
            return;
        }

        // Update presence state
        $late_time = self::get_late_minutes($DB, $lesson_id);
        $result = $DB->execute('update {presenze_eventi} set presence_state = \'P\' , late_time = ' . $late_time . ' where student_id=' . $USER->id . ' and lesson_id=' . $lesson_id);
        if ($result)
            echo json_encode(array('response' => 'presence_marked'));
        else
            echo json_encode(array('error' => 'Event not added'));

    }

    /**
     * Returns description of method parameters
     * @return external_function_parameters
     */
    public static function mark_presence_parameters()
    {
        return new external_function_parameters(
            array(
                'lesson_id' => new external_value(PARAM_TEXT, 'Number that identifies uniquely a lesson'),
            )
        );
    }

    /**
     * Returns description of method result value
     * @return external_description
     */
    public static function mark_presence_returns()
    {
        return new external_value(PARAM_TEXT, 'Return json response with two possible state : error or ok');
    }

    /**
     * @param $DB
     * @param $lesson_id
     * @return int
     */
    private static function get_late_minutes($DB, $lesson_id)
    {
        $lesson = $DB->get_record_sql('SELECT * FROM {presenze_lezioni} WHERE id = ?', array($lesson_id));
        $start_lesson = $lesson->lesson_start_date;
        $now_date = date("Y-m-d H:i:s");
        $first_date = new DateTime($now_date);
        $second_date = new DateTime($start_lesson);
        $interval = $first_date->diff($second_date);
        return $interval->i + $interval->h * 60;
    }

    private static function check_exist_lesson($lesson_id)
    {
        global $DB;
        return $DB->get_record_sql('select * from {presenze_lezioni} where id = ' . $lesson_id);
    }


    private static function check_time_to_mark($lesson_id)
    {
        global $DB;
        $now_date = date("Y-m-d H:i:s");
        return $DB->get_record_sql('select * from {presenze_lezioni} where id = ' . $lesson_id .' and lesson_start_date<= "'.$now_date.'" and "'.$now_date.'" <= DATE_ADD(lesson_start_date,INTERVAL 30 MINUTE)');
    }

    /************************************************
     *
     * //////////////////////////////////
     * // GET ALL COURSES OF A STUDENT //
     * //////////////////////////////////
     *************************************************/

    /**
     * @return string
     * @throws coding_exception
     * @throws dml_missing_record_exception
     * @throws dml_multiple_records_exception
     * @throws invalid_parameter_exception
     * @throws moodle_exception
     * @throws restricted_context_exception
     *
     * Return json with all courses in which current student is enrolled
     */
    public static function get_courses()
    {

        global $USER;
        global $DB;

        //Parameters validation
        self::validate_parameters(self::get_courses_parameters(),
            array()
        );

        //Context validation
        $context = get_context_instance(CONTEXT_USER, $USER->id);
        self::validate_context($context);

        //Capability checking
        if (!has_capability('moodle/user:viewdetails', $context)) {
            throw new moodle_exception('cannotviewprofile');
        }

        $all_student_courses = enrol_get_users_courses($USER->id, true, null, null);

        // Prepare string statement for next db query
        $courses_string = '(';
        $first_time = true;
        foreach ($all_student_courses as $course) {
            if ($first_time) {
                $courses_string .= $course->id;
                $first_time = false;
            } else
                $courses_string .= ',' . $course->id;
        }
        $courses_string .= ')';

        // Select from courses in which student is enrolled, only those where plugin 'presenze' is installed
        $effective_courses = $DB->get_records_sql('SELECT course as id FROM {presenze} WHERE course IN ' . $courses_string);

        $final_courses = array();
        // Create and send json response
        foreach ($effective_courses as $id => $course) {
            $course->name = $all_student_courses[$id]->fullname;
            $final_courses[] = $course;
        }
        echo json_encode($final_courses);
    }

    /**
     * Returns description of method parameters
     * @return external_function_parameters
     */
    public static function get_courses_parameters()
    {
        return new external_function_parameters(
            array()
        );
    }

    /**
     * Returns description of method result value
     * @return external_description
     */
    public static function get_courses_returns()
    {
        return new external_value(PARAM_TEXT, 'Return json with all courses in which current student is enrolled');
    }


    /************************************************
     *
     * /////////////////////////////////////////
     * // GET STUDENT STATISTICS FOR A COURSE //
     * /////////////////////////////////////////
     *************************************************/

    /**
     * @param $course_id
     * @return string in json format statistics for the given student and the given course
     * @throws coding_exception
     * @throws invalid_parameter_exception
     * @throws moodle_exception
     * @throws restricted_context_exception Return in json format statistics for the given student and the given course
     *
     */
    public static function get_course_statistics($course_id)
    {
        global $USER;
        global $DB;

        //Parameters validation
        self::validate_parameters(self::get_course_statistics_parameters(),
            array('course_id' => $course_id)
        );

        //Context validation
        $context = get_context_instance(CONTEXT_USER, $USER->id);
        self::validate_context($context);

        //Capability checking
        if (!has_capability('moodle/user:viewdetails', $context)) {
            throw new moodle_exception('cannotviewprofile');
        }

        // Check if course id exist
        $result = $DB->get_record_sql('select course from {presenze} where course =' . $course_id);

        if (!$result) {
            echo '{"error":"Invalid course id"}';
            return;
        }

        // Calculate statistics fro given student and course id
        $now = date("Y-m-d H:i:s");
        $finished_lessons_number = $DB->get_record_sql('select count(*) as tot from {presenze_lezioni} where course_id=' . $course_id . ' and lesson_end_date <= "' . date('Y-m-d H:i:s') . '"');
        $presences = $DB->get_record_sql('select count(presence_state) as tot from {presenze_eventi},{presenze_lezioni} where {presenze_lezioni}.id = {presenze_eventi}.lesson_id and presence_state = "P" and lesson_end_date <= "' . $now . '"and course_id=' . $course_id . ' and student_id=' . $USER->id . ' group by presence_state');
        $absences = $DB->get_record_sql('select count(presence_state) as tot from {presenze_eventi},{presenze_lezioni} where {presenze_lezioni}.id = {presenze_eventi}.lesson_id and presence_state = "A" and lesson_end_date <= "' . $now . '"and course_id=' . $course_id . ' and student_id=' . $USER->id . ' group by presence_state');
        $excused = $DB->get_record_sql('select count(presence_state) as tot from {presenze_eventi},{presenze_lezioni} where {presenze_lezioni}.id = {presenze_eventi}.lesson_id and presence_state = "E" and lesson_end_date <= "' . $now . '"and course_id=' . $course_id . ' and student_id=' . $USER->id . ' group by presence_state');

        $presences_ratio = round($presences->tot / $finished_lessons_number->tot * 100);
        $absences_ratio = round($absences->tot / $finished_lessons_number->tot * 100);
        $excused_ratio = round($excused->tot / $finished_lessons_number->tot * 100);

        $response = array("course_id" => $course_id, "P" => $presences_ratio, "A" => $absences_ratio, "E" => $excused_ratio);
        echo json_encode($response);
    }

    /**
     * Returns description of method parameters
     * @return external_function_parameters
     */
    public static function get_course_statistics_parameters()
    {
        return new external_function_parameters(
            array(
                'course_id' => new external_value(PARAM_TEXT, 'Integer that identify course id'),
            )
        );
    }

    /**
     * Returns description of method result value
     * @return external_description
     */
    public static function get_course_statistics_returns()
    {
        return new external_value(PARAM_TEXT, 'Return in json format statistics for the given student and the given course');
    }

    /***************************************************
     *
     * /////////////////////////////////////////////
     * // GET CURRENT TOKEN FOR THE GIVEN USER ID //
     * /////////////////////////////////////////////
     ****************************************************/

    /**
     * @param $user_id
     * @throws coding_exception
     * @throws invalid_parameter_exception
     * @throws moodle_exception
     * @throws restricted_context_exception
     */
    public static function get_user_token($user_id)
    {
        global $DB;
        global $USER;

        //Parameters validation
        $params = self::validate_parameters(self::get_user_token_parameters(),
            array(
                'user_id' => $user_id)
        );

        //Context validation
        $context = get_context_instance(CONTEXT_USER, $USER->id);
        self::validate_context($context);

        //Capability checking
        if (!has_capability('moodle/user:viewdetails', $context))
            throw new moodle_exception('cannotviewprofile');

        // Check if the given user id really exists
        $exist_id = $DB->get_record_sql('select id from {user} where id =' . $user_id);
        if (!$exist_id) {
            echo json_encode(array('error' => 'Invalid user id'));
            return;
        }

        // Return token for the given user_id
        $token = $DB->get_record_sql('select token from {external_tokens} where userid =' . $user_id);
        if ($token)
            echo json_encode($token);
        else
            echo json_encode(array('error' => 'Invalid user id'));
    }

    /**
     * Returns description of method parameters
     * @return external_function_parameters
     */
    public static function get_user_token_parameters()
    {
        return new external_function_parameters(
            array(
                'user_id' => new external_value(PARAM_TEXT, 'Id of the user whose token will be returned'),
            )
        );
    }

    /**
     * Returns description of method result value
     * @return external_description
     */
    public static function get_user_token_returns()
    {
        return new external_value(PARAM_TEXT, 'Return in json format the current token for the given user id');
    }
}