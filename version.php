<?php
/**
 * Created by PhpStorm.
 * User: maske
 * Date: 29/03/2016
 * Time: 14:51
 */
$plugin->version = 2016050701;  // The (date) version of this module + 2 extra digital for daily versions
// This version number is displayed into /admin/forms.php
// will not be updated to the current date but just incremented. We will
// need then a $plugin->release human friendly date. For the moment, we use
// display this version number with userdate (dev friendly)
$plugin->requires = 2015111600; // Requires this Moodle version - at least 2.0
$plugin->cron = 0;
$plugin->component = 'local_presenzewebservice'; // Declare the type and name of this plugin.
$plugin->release = '1.0 (Build: 2016031900)';
$plugin->maturity = MATURITY_STABLE;