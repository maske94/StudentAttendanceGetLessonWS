<?php
/**
 * Created by PhpStorm.
 * User: maske
 * Date: 29/03/2016
 * Time: 15:21
 */

// We defined the web service functions to install.
$functions = array(
    'local_presenzewebservice_get_lesson_by_beacon' => array(
        'classname'   => 'local_presenzewebservice_external',
        'methodname'  => 'get_lesson_by_beacon',
        'classpath'   => 'local/presenzewebservice/externallib.php',
        'description' => 'Return lesson tht user should attend in that moment',
        'type'        => 'read',
    ),
    'local_presenzewebservice_mark_presence' => array(
        'classname'   => 'local_presenzewebservice_external',
        'methodname'  => 'mark_presence',
        'classpath'   => 'local/presenzewebservice/externallib.php',
        'description' => 'Store new presence event',
        'type'        => 'write',
    ),
    'local_presenzewebservice_get_courses' => array(
        'classname'   => 'local_presenzewebservice_external',
        'methodname'  => 'get_courses',
        'classpath'   => 'local/presenzewebservice/externallib.php',
        'description' => 'Return in json format all courses in which given student is enrolled',
        'type'        => 'read',
    ),
    'local_presenzewebservice_get_course_statistics' => array(
        'classname'   => 'local_presenzewebservice_external',
        'methodname'  => 'get_course_statistics',
        'classpath'   => 'local/presenzewebservice/externallib.php',
        'description' => 'Return in json format statistics for the given student and the given course',
        'type'        => 'read',
    ),
    'local_presenzewebservice_get_user_token' => array(
        'classname'   => 'local_presenzewebservice_external',
        'methodname'  => 'get_user_token',
        'classpath'   => 'local/presenzewebservice/externallib.php',
        'description' => 'Return in json format the current token for the given user id',
        'type'        => 'read',
    )
);

// We define the services to install as pre-build services. A pre-build service is not editable by administrator.
$services = array(
    'Web Service Presence' => array(
        'functions' => array ('local_presenzewebservice_get_lesson_by_beacon','local_presenzewebservice_mark_presence','local_presenzewebservice_get_courses','local_presenzewebservice_get_course_statistics','local_presenzewebservice_get_user_token'),
        'restrictedusers' => 0,
        'enabled'=>1,
    )
);